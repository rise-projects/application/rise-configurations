# RISE Configurations

This repository houses a collection of XML and JSON configuration files for the RISE system. These files provide a set of predefined configurations that can be used to fine-tune and customize the RISE system for various applications and environments.

There are three different configurations directories in each setup folder:
- CommunicationRobotActs (CRA) in **XML** format
- InteractionRules (IR) in **XML** format
- WorkingMemory (WM) in **JSON** format

Additionally, within the validation section, you will discover XSD files employed by RISE for the purpose of validating the accuracy of XML files.

CommunicationRobotActs, InteractionRules, and Working Memory are used to control a robot using the RISE system. Finally, there is a configuration mentioned in this Tutorial [How to create a Human-Robot-Interaction Scenario](https://rise-projects.pages.ub.uni-bielefeld.de/rise-documentation/tutorials/tutorialScenario.html).

<br>
For more information, please refer to the official [RISE documentation](https://rise-projects.pages.ub.uni-bielefeld.de/rise-documentation/contents.html).